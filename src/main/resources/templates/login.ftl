<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
<div class="page-header">
    <h2>Страница авторизации</h2>
</div>
<@l.login "/login" false/>

<div class="page-header">
    <h2>Страница регистрации</h2>
</div>
<a class="btn btn-primary" href="/registration" role="button">Добавить нового пользователя</a>

</@c.page>
