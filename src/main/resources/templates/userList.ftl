<#import "parts/common.ftl" as c>

<@c.page>
<div class="page-header">
    <h2>Список зарегистрированыых пользователей</h2>
</div>

<table class="table">
    <thead>
    <tr>
        <th>ФИО</th>
        <th>email</th>
        <th>login</th>
        <th>role</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list users as user>
    <tr>
        <td>${user.userLastFirstName?ifExists}</td>
        <td>${user.email?ifExists}</td>
        <td>${user.username}</td>
        <td><#list user.roles as role>${role}<#sep>, </#list></td>
        <td><a href="/user/${user.id}">Редактировать</a></td>
    </tr>
    </#list>
    </tbody>
</table>

<div class="page-header">
    <h2>Добавить нового пользователя</h2>
</div>
<a class="btn btn-primary" href="/registration" role="button">Добавить нового пользователя</a>

</@c.page>