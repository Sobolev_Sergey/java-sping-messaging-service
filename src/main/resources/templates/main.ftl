<#include "parts/security.ftl">
<#import "parts/common.ftl" as c>

<@c.page>
<div class="page-header">
    <h3>Поиск по теме</h3>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <form method="get" action="/main" class="form-inline">
            <input type="text" name="filter" class="form-control" value="${filter?ifExists}" placeholder="Тема">
            <button type="submit" class="btn btn-primary ml-2">Найти</button>
        </form>
    </div>
</div>

<div class="page-header">
    <h2>Список сообщений в табличной форме</h2>
</div>
<table class="table">
    <thead>
    <tr>
        <th>От кого</th>
        <#if isAdmin>
            <th>Кому</th>
        </#if>
        <th>Дата - время</th>
        <th>Тема</th>
        <th>Текст</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list messages as message>
    <tr>
        <td>${message.sender?ifExists}</td>
        <#if isAdmin>
            <td>${message.recipient?ifExists}</td>
        </#if>
        <td>${message.dateTime?ifExists}</td>
        <td>${message.tag?ifExists}</td>
        <td>${message.text?ifExists}</td>
        <td>Редактировать</td>
    </tr>
    <#else>
        No message
    </#list>

    </tbody>
</table>

   <b>  | Удалить сообщение </b>
   <b>  | Сортировать сообщения по полям </b>


<div class="page-header">
    <h2>Адресная книга.</h2>
</div>
<table class="table table-hover">
    <thead class="thead-inverse">
    <tr>
        <th>ФИО</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <#list users as user>
    <tr>
        <td>${user.userLastFirstName?ifExists}</td>
        <td>
            <div class="container mt-30">
                <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    Создать новое сообщение
                </a>
                <div class="collapse" id="collapseExample">
                    <div class="form-group mt-3">
                        <form method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" class="form-control" name="recipient" value="${user.userLastFirstName?ifExists}" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="tag" placeholder="Тема" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="text" placeholder="Введите сообщение" />
                            </div>
                            <input type="hidden" name="_csrf" value="${_csrf.token}" />
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Отправить сообщение</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </td>
    </tr>
    </#list>
    </tbody>
</table>


    <b> ФИО - удалить - отправить ему сообщение </b>
    <div> добавить пользователя </div>

<div> Ссылка и всплывающий диалог для смены пароля </div>

</@c.page>