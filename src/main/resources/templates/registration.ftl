<#import "parts/common.ftl" as c>

<@c.page>
<div class="page-header">
    <h2>Страница регистрации</h2>
</div>

${message?ifExists}
<form action="/registration" method="post">
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">ФИО :</label>
        <div class="col-sm-6">
            <input type="text" name="userLastFirstName" class="form-control" placeholder="ФИО" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">E-mail :</label>
        <div class="col-sm-6">
            <input type="text" name="email" class="form-control" placeholder="E-mail" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Имя пользователя :</label>
        <div class="col-sm-6">
            <input type="text" name="username" class="form-control" placeholder="Имя пользователя" />
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-2 col-form-label">Пароль:</label>
        <div class="col-sm-6">
            <input type="password" name="password" class="form-control" placeholder="Пароль" />
        </div>
    </div>
    <input type="hidden" name="_csrf" value="${_csrf.token}" />

    <button class="btn btn-primary" type="submit">Добавить нового пользователя</button>
</form>

</@c.page>
