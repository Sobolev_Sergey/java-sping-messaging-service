package my.example.sermes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * запуск приложения
 * SpringBootApplication - эта аннотация проверяет все модели, контроллеры,
 * подключение к базе данных и запускает
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}