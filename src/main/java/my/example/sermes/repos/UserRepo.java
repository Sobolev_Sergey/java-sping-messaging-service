package my.example.sermes.repos;

import my.example.sermes.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 */
public interface UserRepo extends JpaRepository<User, Long> {
    /**
     * Find user
     *
     * @param username The user name
     * @return user
     */
    User findByUsername(String username);
}