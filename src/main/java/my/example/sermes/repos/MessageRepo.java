package my.example.sermes.repos;

import my.example.sermes.domain.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

/**
 *
 */
public interface MessageRepo extends CrudRepository<Message, Long> {

    /**
     * Руководство по JPA репозиториям:
     * https://docs.spring.io/spring-data/jpa/docs/1.5.0.RELEASE/reference/html/
     * jpa.repositories.html#jpa.query-methods.query-creation
     *
     * @param tag
     * @return
     */
    List<Message> findByTag(String tag);
}
